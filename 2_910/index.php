<?php

//--------------------------Настройки подключения к БД-----------------------
$db_host = 'std-mysql.ist.mospolytech.ru';
$db_user = 'std_2002_bd'; //имя пользователя совпадает с именем БД
$db_password = '12345678'; //пароль, указанный при создании БД
$database = 'std_2002_bd'; //имя БД, которое было указано при создании
$link = mysqli_connect($db_host, $db_user, $db_password, $database);
if ($link == False) {
    die("Cannot connect DB");
}

//----------------------------------------------------------------------------------------
function update_rele($link, $id) {
    if(isset($_POST['button_on'])) {
        $status = 1;
    } 
    else $status = 0;
    $date_today = date("Y-m-d H:i:s");
    $query = "UPDATE out_state_table SET out_state = '$status', date_time= '$date_today' WHERE device_id = '$id'";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) != 1) //Если не смогли обновить - значит в таблице просто нет данных о команде для этого устройства
    { //вставляем в таблицу строчку с данными о команде для устройства
        $query = "INSERT out_state_table SET device_id = '$id', out_state= '$status', date_time= '$date_today'";
        $result = mysqli_query($link, $query);
    }
}
function add_command($link, $id) {
    if(isset($_POST['button_on'])) {
        $status = 'rele_on';
    } 
    else $status = 'rele_off';
    $date_today = date("Y-m-d H:i:s");
    $query = "INSERT device_action SET device_id = '$id', command = '$status', date_today='$date_today'";
    $result = mysqli_query($link, $query);
}
if (isset($_POST['button_on'])) {
    $id = $_POST['button_on'];
    $date_today = date("Y-m-d H:i:s");
    $query = "UPDATE command_table SET command='1', date_time= '$date_today' WHERE device_id = '$id'";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) != 1) //Если не смогли обновить - значит в таблице просто нет данных о команде для этого устройства
    { //вставляем в таблицу строчку с данными о команде для устройства
        $query = "INSERT command_table SET device_id= '$id', command='1', date_time='$date_today'";
        $result = mysqli_query($link, $query);
    }
    update_rele($link, $id);
    add_command($link, $id);
}

if (isset($_POST['button_off'])) {
    $id = $_POST['button_off'];
    $date_today = date("Y-m-d H:i:s");
    $query = "UPDATE command_table SET command='0', date_time='$date_today' WHERE device_id = '$id'";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) != 1) //Если не смогли обновить - значит в таблице просто нет данных о команде для этого устройства
    { //вставляем в таблицу строчку с данными о команде для устройства
        $query = "INSERT command_table SET device_id='$id', command='0', date_time='$date_today'";
        $result = mysqli_query($link, $query);
    }
    update_rele($link, $id);
    add_command($link, $id);
}


//-----------------------------------------------------------------------

echo '
<!DOCTYPE HTML>
<html id="App_interface">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MyApp</title>

</head>
<body>
<table> ';

$query = "SELECT device_id FROM device_table";
$resultABS = mysqli_query($link, $query);
while($line = mysqli_fetch_array($resultABS)){
$id = $line['device_id'];
//-----------------Получаем из БД все данные об устройстве-------------------
$query = "SELECT * FROM device_table WHERE device_id = '$id'";
$result = mysqli_query($link, $query);
if (mysqli_num_rows($result) == 1) { //Если в БД есть данные о имени для этого устройства
    $Arr = mysqli_fetch_array($result);
    $device_name = $Arr['name'];
} else { //Если в БД нет данных о имени для этого устройства
    $device_name = $Arr['name'];
}

$query = "SELECT * FROM temperature_table WHERE device_id = '$id'";
$result = mysqli_query($link, $query);
if (mysqli_num_rows($result) == 1) { //Если в БД есть данные о температуре для этого устройства
    $Arr = mysqli_fetch_array($result);
    $temperature = $Arr['temperature'];
    $temperature_dt = $Arr['date_time'];
} else { //Если в БД нет данных о температуре для этого устройства
    $temperature = '?';
    $temperature_dt = '?';
}

$query = "SELECT * FROM out_state_table WHERE device_id = '$id'";
$result = mysqli_query($link, $query);
if (mysqli_num_rows($result) == 1) { //Если в БД есть данные о реле для этого устройства
    $Arr = mysqli_fetch_array($result);
    $out_state = $Arr['out_state'];
    $out_state_dt = $Arr['date_time'];
} else { //Если в БД нет данных о реле для этого устройства
    $out_state = '?';
    $out_state_dt = '?';
}

//----------------------------------------------------------------------------------------



//-------Формируем интерфейс приложения для браузера---------------------
echo '
<tr>
<td width=100px> Устройство:
</td>
<td width=40px><a href="/device_action.php?id='.$id.'">'. $device_name . '</a>
</td>
</tr>
</table>

<table border=1>
<tr>
<td width=100px> Tемпература
</td>
<td width=40px>' . $temperature . '
</td>
<td width=150px>' . $temperature_dt . '
</td>
</tr>
<tr>
<td width=100px> Реле
</td>
<td width=40px>' . $out_state . '
</td>
<td width=150px> ' . $out_state_dt . '
</td>
</tr>
</table>

<form>
<button formmethod="POST" name="button_on" value='.$id.'>Включить реле</button>
</form>
<form>
<button formmethod="POST" name="button_off" value='.$id.'>Выключить реле</button>
</form>
';
//----------------------------------------------------------------------
}

echo '
<form name="add" action="reg.php" method="POST">
    <h2>Добавить девайс</h2>
    <p>Введите название:</p>
  <p><input type="text" name="name" size="40" value="devicename"></p>
  <p>Введите пароль:</p>
  <p><input type="password" name="password" maxlength="25" size="40" name="password"></p>
  <input type = "submit">
</form>
</body>
</html>';
?>